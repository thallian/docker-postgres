[PostgreSQL](https://www.postgresql.org/) server.

# Volumes
- `/var/lib/postgresql/data`

# Environment Variables
## POSTGRES_PASSWORD

Password for the postgre admin user.

# Ports
- 5432
