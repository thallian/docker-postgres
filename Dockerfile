FROM registry.gitlab.com/thallian/docker-confd-env:master

ENV PGDATA /var/lib/postgresql/data

RUN apk add --no-cache postgresql postgresql-contrib

RUN mkdir -p /run/postgresql && mkdir -p $PGDATA
RUN chown -R postgres /run/postgresql && chown -R postgres $PGDATA
RUN chmod 775 /run/postgresql

ADD /rootfs /

VOLUME /var/lib/postgresql/data /etc/ssl/postgresql/

EXPOSE 5432
